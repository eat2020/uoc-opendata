# UOC-Opendata

Opendata weather collection platform ESP32 node implementation using PlatformIO.

In order to register a new home weather collection node please sign up at [https://fos.cmb.ac.lk/opendata/esp/v2/signUp/](https://fos.cmb.ac.lk/opendata/esp/v2/signUp/)


## Pin configuration 

 ESP32 PIN | Description 
--- | --- 
4 | DHT11
 | DS18B20


## Features to be added

- [x] ESP32 weather station node using DHT11 and DS18B20
- [ ] Irrediance monitoring - CM11
- [ ] Multicore operation for OTA programming
