

//User configuration file 

//Update the settings according to your configuration 

//Keep this file private (Not to be uploaded to git)
const char* ssid       = "SSID";
const char* password   = "Password for wifi";


// Get your own API key by registering at https://fos.cmb.ac.lk/opendata/esp/v2/signUp/
// Donot share the API key

const char API_KEY[50] = "API_Key";