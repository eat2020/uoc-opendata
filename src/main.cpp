#include <Arduino.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <WiFi.h>

#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <HTTPClient.h>

#include <WiFiUdp.h>

#include <NTPClient.h>
#include <TimeLib.h>


const long  gmtOffset_sec = 0;
const int   daylightOffset_sec = 0;

//Modify the use config file as needed. Donot upload 
#include "user_config.h"

#define LED 2

float DHT_t,DHT_h;

#define DHTPIN 4     
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);

WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP);
time_t t;


void update_time(){
  int count = 0;
  Serial.println("Connecting NTP");

  timeClient.begin();
  timeClient.setTimeOffset(0);
  while(!timeClient.update()){
    timeClient.forceUpdate();
    Serial.printf(".");
    count ++;
  }
  if (count<500){
    Serial.printf("\n");
    setTime(timeClient.getEpochTime());
    Serial.printf("Time updated\n");
    t = timeClient.getEpochTime();
    Serial.println(t);
  }else{

    Serial.printf("Time not set\n");
  }
    
}

#define temp 1
#define humidity 0

void upload(int sensor_type){
  HTTPClient http;
  char data[300];
    

    t = now();
    
    if (sensor_type == temp){
      sprintf(data,"https://fos.cmb.ac.lk/opendata/esp/v2/get_data.php?api_key=%s&sensor=1&location=4&Parameter=%s&Value=%.2f&Reading_Time=%d",API_KEY,"Tempareture",DHT_t,t);
    }
    if (sensor_type == humidity){
      sprintf(data,"https://fos.cmb.ac.lk/opendata/esp/v2/get_data.php?api_key=%s&sensor=1&location=4&Parameter=%s&Value=%.2f&Reading_Time=%d",API_KEY,"Humidity",DHT_h,t);
    }
  //#define debug

  #ifndef debug 
    http.begin(data);
    int httpResponseCode = http.GET();
     
   
    if (httpResponseCode>0) {
      Serial.print("HTTP Response code: ");
      Serial.println(httpResponseCode);
    }
    else {
      Serial.print("Error code: ");
      Serial.println(httpResponseCode);
    }
    // Free resources
    http.end();
  #endif
    
}

void start_OTA(){
  
  ArduinoOTA.setPort(3232);
  Serial.printf("OTA start on 3232\n");

  ArduinoOTA.onStart([]() {
      String type;
      if (ArduinoOTA.getCommand() == U_FLASH)
        type = "sketch";
      else // U_SPIFFS
        type = "filesystem";
      // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
      Serial.println("Start updating " + type);
    });
  ArduinoOTA.onEnd([]() {
      Serial.println("\nEnd");
    });

  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
      Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });

  ArduinoOTA.onError([](ota_error_t error) {
      Serial.printf("Error[%u]: ", error);
      if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
      else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
      else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
      else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
      else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
  ArduinoOTA.begin();
}

void init_io(){

  pinMode(LED,OUTPUT);

}

void connect_wifi(){
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  int count = 0;
    while (WiFi.status() != WL_CONNECTED  ) {
        
        digitalWrite(LED,HIGH);
        delay(10);
        digitalWrite(LED,LOW);  
        delay(290);
        Serial.printf("%d\n",count++);
        delay(300);
    }
    
    Serial.println(WiFi.localIP());
}

void read_DHT(){

  while(1){

  // delay(2000);
    DHT_h = dht.readHumidity();
    DHT_t = dht.readTemperature();
    if (isnan(DHT_h) || isnan(DHT_t)) {
      Serial.println(F("DHT Error"));
    }
    Serial.printf("%.2f,%.2f\n",DHT_h,DHT_t);
    return;
    
    }
}

#define uS_TO_S_FACTOR 1000000  /* Conversion factor for micro seconds to seconds */
#define TIME_TO_SLEEP  300      /* Time ESP32 will go to sleep (in seconds) */

void setup() {
  Serial.begin(115200);
  // put your setup code here, to run once:
  init_io();
  connect_wifi();
  start_OTA();
  
  digitalWrite(LED,HIGH);
  update_time();
  digitalWrite(LED,LOW);

/*

  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
*/



}

void loop() {
  // put your main code here, to run repeatedly:
  
  digitalWrite(LED,HIGH);
  update_time();
  dht.begin();
  read_DHT();
  upload(temp); 
  upload(humidity);
  digitalWrite(LED,LOW);

/*

  WiFi.disconnect(true);
  WiFi.mode(WIFI_OFF);
*/
  esp_sleep_enable_timer_wakeup(TIME_TO_SLEEP * uS_TO_S_FACTOR);
  Serial.println("Sleeping");
  esp_light_sleep_start();
  //ArduinoOTA.handle();
 
}